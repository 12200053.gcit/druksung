from django.shortcuts import render, HttpResponse, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.db import IntegrityError
from .models import Feedback


def home(request):
    return render(request, 'home.html')

@login_required
def userhome(request):
        if request.method == 'POST':
            name=request.POST.get('name')
            email=request.POST.get('email')
            subject=request.POST.get('subject')
            message=request.POST.get('message')
            feedback=Feedback(email=email,message=message,name=name,subject=subject)
            feedback.save()
            print(feedback)
        return render(request, 'userhome.html')
    
def register(request):
    if request.method == 'POST':
        uname = request.POST.get('username')
        email = request.POST.get('email')
        pass1 = request.POST.get('password1')
        pass2 = request.POST.get('password2')

        if pass1 != pass2:
            error_message = " password & confirm password does not match!!"
        else:
            try:
                my_user = User.objects.create_user(uname, email, pass1)
                my_user.save()
                return redirect('login')
            except IntegrityError:
                error_message = "Username already exists"

        context = {
            'error_message': error_message
        }
        return render(request, 'register.html', context)

    return render(request, 'register.html')


def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('pass')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('userhome')
        else:
            context = {'error': 'Username or Password is incorrect!!!'}
            return render(request, 'login.html', context)

    return render(request, 'login.html')


def logout_view(request):
    logout(request)
    return redirect('home')

def my_view(request):
    context = {'user': request.user}
    return render(request, 'userhome.html', context)

