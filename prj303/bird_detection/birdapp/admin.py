from django.contrib import admin
from .models import Feedback,BirdRecord

admin.site.register(Feedback)

class ImageAdmin(admin.ModelAdmin):
    list_display=('image',)

admin.site.register(BirdRecord, ImageAdmin)
